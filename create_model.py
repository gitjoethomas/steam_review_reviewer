import pandas as pd
import spacy
import importlib
nlp = spacy.load('en_core_web_sm')
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from statistics import mean
import pickle
import yaml
from operator import itemgetter

import bespoke_functions
f = bespoke_functions.SteamReviewClassifier()

# this is how we train the model. 
def train_model(df, reg, ngram_range = (1,2)):
    """
    trains the supplied model on the supplied dataframe. Expects the model to have at least two columns, named 'text' and
    'voted_up'. You can also adjust the ngram_range to add word pairs/trios to the mode's features.
    """
    
    # remove stopwords ("and", "if" etc.). using a modified list of stopwords
    df['no_stopwords'] = df['text'].apply(f._remove_stopwords) 
    df['lemmas'] = df['no_stopwords'].apply(f._lemmatize) # convert all words to their root word (removes plurals, tenses etc.)
    
    # convert our review dataframe to a matrix. the columns are each word available. rows are reviews. a 1 indicates the 
    # review has that word. This is actually a sparse matrix, but it's essentially the same.
    cv = CountVectorizer(binary=True, ngram_range = ngram_range)
    cv.fit(df['lemmas'])
    train = cv.transform(df['lemmas'])

    # this is the y value
    result = df['voted_up'].copy()
    result = result.replace({True : 1, False : 0})
    
                                        ### validation - confusion matrix ###
    
    # cross validation, to be sure we don't overfit. We're using statified k-fold, so we're sure the folds have the same number of positives, and we'll be able to take the mean of the results.
    precisions, recalls = f._cross_validated_confusion_matrix_outcomes(reg, train, result, 5)

    # mean scores - we'll be reporting these.
    mean_precision = round(mean(precisions), 3)
    mean_recall = round(mean(recalls), 3)
    f_score = round(((mean_precision * mean_recall) / (mean_precision + mean_recall)) * 2,3)

    print("mean precision is "+str(mean_precision))
    print("mean recall is "+str(mean_recall))
    print("f score is "+ str(f_score))
    
    
                                        ### positive and negative words ###
        
    # relative feature importances
    coefficients = list(zip(cv.get_feature_names(), reg.coef_[0]))
    sorted_coefficients = sorted(coefficients, key = itemgetter(1))

    # top and bottom words, so we can say what the key words are later. the list comprehension is unpacking words from coefficients
    most_negative_words = [word[0] for word in sorted_coefficients[:200]]
    most_positive_words = [word[0] for word in sorted_coefficients[-200:][::-1]]
    
    worddict = {'positive':most_positive_words, 'negative':most_negative_words}
    
    return reg, cv, worddict

def create_model():
    print("beginning... runtime is 3-4 minutes")

    df = pd.read_csv("datafiles/original_dataset.csv", dtype = {"text":str, "voted_up":bool})

    # selected 0.7 because gridsearching gave the best confusion matrix F score
    reg = LogisticRegression(C = 0.7, random_state = 42, solver = "lbfgs")

    # remove stopwords, ngrams, lemmatise, convert to sparse matrix and run stratified k-fold cross validation
    reg, cv, positives_and_negatives = train_model(df, reg) # function will also print out validation metrics

                                                    ### EXPORTS ###

    while True:
        flag = input("considering these outputs, dump model, vectorizor and key words to datafiles/? (Y/N) ")
        if flag.upper() in ["Y", "N"]:
            break
        else:
            print("you must pass either Y or N")

    if flag.upper() == "Y":

        with open("datafiles/CountVectorizor.pkl", "wb") as file:
            pickle.dump(cv, file)
        print("instance of CountVectorizor has been dumped to datafiles/CountVectorizor.pkl")

        with open("datafiles/LogisticRegression.pkl", "wb") as file:
            pickle.dump(reg, file)
        print("instance of LogisticRegression has been dumped to datafiles/LogisticRegression.pkl")

        with open("datafiles/PositivesNegatives.yaml", "w+") as file:
            yaml.dump(positives_and_negatives, file)
        print("positive and negative words have been dumped to datafiles/PositivesNegatives.yaml")

    else:
        print("model instances were not dumped - aborted")