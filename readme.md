### Steam review evaluator

assess_game.py will run with `python assess_game.py`, and will request an app_id, and the number of reviews to get. For this game, code will query the steam API for this many reviews, export dataframe of reviews, and apply model to assess reviews. Next, the model splits out key positive and negative words, and exports modelled dataset.

The model can be retrained with `create_model.py`, and will need to be modified within the file. The code will print out performance metrics, and ask if you wish to replace existing pickle files with the ones generated in this run.