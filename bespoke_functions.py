import requests
import json
import time
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
from operator import itemgetter
import pickle
import re
import yaml
import spacy
nlp = spacy.load('en_core_web_sm')

# for word in ['not', 'nobody', 'nothing', 'nowhere', 'now', 'only', 'no', 'none', 'noone',  'never', 'least', 'less', 'empty', 'cannot', 'alone']:
#     nlp.Defaults.stop_words.remove(word)



class SteamReviewClassifier():
    
    def __init__(self, recreate_model = False):
        
        """
        performs end to end sentiment analysis on a given game (referenced by a Steam App ID). If recreate_model is True, model will re-create itself using a dataframe in the datafiles/ directory, expecting columns called 'text' and 'voted up'
        
        """
        if recreate_model == True:
            create_model.create_model()
            
        with open("datafiles/CountVectorizor.pkl", "rb") as file:
            cv = pickle.load(file)
        print("CountVectorizor imported")

        with open("datafiles/LogisticRegression.pkl", "rb") as file:
            reg = pickle.load(file)
        print("LogisticRegression model imported")

        with open("datafiles/PositivesNegatives.yaml", "r+") as file:
            positives_and_negatives = yaml.load(file, Loader = yaml.FullLoader)
        print("key positive and negative words imported")
    
    def _remove_stopwords(self, string):
        
        """returns the contents of the supplied cell, minus the stopwords"""
        
        try:           
            string = string.lower()
            re_phrase = re.sub(r'W+', ' ', string)
            re_phrase = re_phrase.replace("!","")
            re_phrase = re_phrase.replace(".","")
            re_phrase = re_phrase.replace(",","")

            # take out stopwords
            result = " ".join([word for word in re_phrase.split() if word not in nlp.Defaults.stop_words and word.isalpha()]) 
        except (AttributeError, TypeError): # some cells look like decimals - they probably have no useful text
            result = "" # if the word can't be converted (because it's all junk, set it to an empty string so lemmatization will still work

        return result
    
    def _lemmatize(self, string):
        
        """returns the contents of the supplied cell, converting all words to their lemmas (roots)"""

        try:
            tokenized = nlp(string)
            result = " ".join(str(token.lemma_) for token in tokenized) # take out stopwords
        except TypeError:
            raise TypeError(string)

        return result
    
    def _cross_validated_confusion_matrix_outcomes(self, model, x, y, num_splits):
        """
        runs sklearn.model_selection.StratifiedKFold, and handles the slicing etc. 
        returns two values. mean accuracy, and mean recall
        """
        folder = StratifiedKFold(n_splits = 5)

        precision_list = []
        recall_list = []

        for train_index, test_index in folder.split(x, y): # generate indices for slicing

            # slice both train and test to ensure a stratified outcome
            train_x, train_y = x[train_index], y[train_index]
            test_x, test_y = x[test_index], y[test_index]

            # generate predictions
            model.fit(train_x, train_y)
            preds = model.predict(test_x)

            # pull out the four outcomes from the confusion matrix
            c_matrix = confusion_matrix(test_y, preds) # this isn't printing in the typical confusion matrix order
            tn = c_matrix[0][0]
            fp = c_matrix[0][1]
            fn = c_matrix[1][0]
            tp = c_matrix[1][1]
            
            precision = tp / (tp + fp) # precision = times you correctly predicted +ve, divided by number of times you guessed +ve
            recall = tp / (tp + fn) # recall times you correctly predicted +ve, divided by the number of +ves there actually were
                        
            precision_list.append(precision)
            recall_list.append(recall)

        return precision_list, recall_list
    
    @staticmethod
    def _filter_for_words(string, wordlist):
                
        """returns the words that are in string that are also in wordlist"""
        
        keywords = " ".join([word for word in string.split() if word in wordlist])
        
        return keywords 
    
    def _make_request(appid, params):
        '''Helper function that sends a request to the Steam Web API and returns the response object.\n
        **appid** -- The Steam App ID obtained from the game's Store page URL\n
        **params** -- An object used to build the Steam API query. (https://partner.steamgames.com/doc/store/getreviews)
        '''
        response = requests.get(f"https://store.steampowered.com/appreviews/{appid}", params=params) # get the data from the endpoint
        response = response.json() # return data extracted from the json response

        reviews = response['reviews']
        reviewlist = []

        for review in range(len(reviews)):
            reviewlist.append(reviews[review]['review'])

        return reviewlist
    
    def _make_many_requests(app_id, num_reviews):
        """
        runs make_requests() to get as many reviews you want. there are 20 reviews per page, so it iterates over pages accordingly
        """
        num_pages = round((num_reviews + (20 - (num_reviews % 20))) / 20) # rounds up to the nearest number of pages

        results = [] # we'll make multiple requests, 
        params = {'json': 1,
                  'filter': 'recent', # sort by: recent, update
                  'language': 'english', # languages at https://partner.steamgames.com/doc/store/localization
                  'start_offset': 0, # for pagination
                  'review_type': 'all', # all, positive, negative
                  'purchase_type': 'all'} # all, non_steam_purchase, steam

        for page in range(num_pages):
            params['start_offset'] = page
            reviews = _make_request(app_id, params)
            results.extend(reviews)

        return results